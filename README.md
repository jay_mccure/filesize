# John's Agile Pi : Raspberry-Pi Ecosystem

I use Ansible to deploy **John's Raspberry Pi ecosystem**,<br>
a full-fledged Ubuntu Linux server enviroment, on this hardware:<br>
Raspberry-PI 4, **power setup** : 8 Gb RAM, large-capacity USB SSD disk(s).

- https://gitlab.com/John_DB/Johns-ansible-public


## Topics

**Argon case**, with fan control software

- https://gitlab.com/umi-ch/ansible/-/tree/main/playbooks/files/pi-argon

**Minecraft server**

- https://gitlab.com/umi-ch/ansible/-/tree/main/playbooks/files/minecraft

**Ubuntu Honeypot**<br>
Show passwords of failed SSH logins, from Internet hackers

- https://gitlab.com/umi-ch/ansible/-/tree/main/playbooks/files/sshd-jdb.zSrc

**Small statistical scripts**:<br>

- https://gitlab.com/umi-ch/ansible/-/tree/main/playbooks/files/pi-heat-logger
- https://gitlab.com/umi-ch/ansible/-/tree/main/playbooks/files/speedtest
- https://gitlab.com/umi-ch/ansible/-/tree/main/playbooks/files/zfs-logger

**Installation** with John's Agile Ansible:<br>

- https://gitlab.com/umi-ch/ansible/-/blob/main/zDocs/2022-11-26.Ansible_Install_cow-pi.home.umi.ch.txt

**Miniconda / Mambaforge** : Variations for dynamic environments<br>

- https://gitlab.com/umi-ch/ansible/-/tree/main/playbooks/files/conda.zPkgs

<br>


## Pictures

**Front view**

[<img src="./zPix/cherry-pi.front.IMG_0727.jpg" width="400" />](./zPix/cherry-pi.front.IMG_0727.jpg)

<br>

**Rear view**<br>
With two Samsung USB 128 Gb "stubby" disks.<br>

[<img src="./zPix/cherry-pi.front.IMG_0728.jpg" width="400" />](./zPix/cherry-pi.front.IMG_0728.jpg)

<br>

**Top view**<br>
With GPIO access panel removed.

[<img src="./zPix/cherry-pi.top.IMG_0736.jpg" width="400" />](./zPix/cherry-pi.top.IMG_0736.jpg)

<br>

**Assembling the case**

[<img src="./zPix/argon.case.IMG_0695.jpg" width="400" />](./zPix/argon.case.IMG_0695.jpg)

<br>
<br>


## Agile theme
This repository has useful working examples, quickly and iteratively adaptable to new environments.
<br/>


## License
Copyright &copy; 2019, Mountain Informatik GmbH<br>
All rights reserved.<br>
Permission to share is granted under the terms of the **Server Side Public License**<br>
https://www.mongodb.com/licensing/server-side-public-license
<br>
