#!/bin/bash

sym_ver='1.1.0'

# - Make backups of selected areas:  /var/log/...
#   Weekly interval recommended.

function dihp() {
	printf '\n-$ date; whoami; uname -n; pwd\n'
       	date; whoami; uname -n; pwd; echo
}
function isodate() {
	date +%Y-%m-%d
}


dihp


## bdir=~/backups/
bdir=/tmp/backups-pi/

host=$(hostname)
if [[ ! -d "${bdir}" ]]; then
	echo "-\$ mkdir -p  ${bdir}"
	mkdir -p "${bdir}"
fi

echo
echo "-\$ bak=~${bdir}/${host}.backup.$(isodate).tgz"
bak="${bdir}"/${host}.backup.$(isodate).tgz
touch ${bak}
chmod 600 ${bak}

echo "-\$ cd ${bdir}"
cd "${bdir}"

echo
echo '-$ time sudo bash -c "cd /; sudo tar czf ${bak} ./etc ./usr/local/etc  ~root/.??*  ~ubuntu/.??*  ~jdb/.??* /var/www /var/log"'
time sudo bash -c "cd /; tar czf ${bak} --exclude='.cache' --exclude='.cpan' ./etc ./usr/local/etc   ~root/.??*  ~ubuntu/.??*  ~jdb/.??*  /var/www /var/log"

echo
echo '-$ ls -lhF'
ls -lhF

echo
echo '-$ tar tzvf ${bak} | head'
tar tzvf ${bak} | head

echo
echo '-$ tar tzvf ${bak} | grep fail'.log
tar tzvf ${bak} | grep fail.log


echo
echo '#  - Transfer the backup file offsite, then remove it.'
echo
echo "-> \$ sudo rm ${bak}"
echo



